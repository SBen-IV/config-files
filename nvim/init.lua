-- lazy.nvim

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins")
require("lualine").setup({ options = { theme = "onedark", section_separators = '', component_separators = '' } })
require("mason").setup({})

-- Other config
vim.o.smarttab = true
vim.o.number = true
--vim.o.termguicolors = true

-- Toggle number when in insert mode
local numbertoggle = vim.api.nvim_create_augroup('numbertoggle', { clear = true })

vim.api.nvim_create_autocmd({ 'BufEnter', 'FocusGained', 'InsertLeave' }, {
	group = numbertoggle,
	command = 'set relativenumber'
})

vim.api.nvim_create_autocmd({ 'BufLeave', 'FocusLost', 'InsertEnter' }, {
	group = numbertoggle,
	command = 'set norelativenumber'
})


-- Restore cursor when leave nvim
local vimleave = vim.api.nvim_create_augroup('vimleave', { clear = true })

vim.api.nvim_create_autocmd({ 'VimLeave' }, {
	group = vimleave,
	command = 'set guicursor=a:ver90'
})

