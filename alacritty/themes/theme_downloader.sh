#!/bin/bash

THEME=$1

if [[ -z $THEME ]]; then
	echo "Usage: ./theme_downloader <theme name>"
	exit 1
fi

curl -o $THEME.yaml https://raw.githubusercontent.com/alacritty/alacritty-theme/master/themes/$THEME.yaml
